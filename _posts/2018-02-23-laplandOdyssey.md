---
layout: post
title: "Lapland Odyssey"
date: 2018-02-26 8:00:00 +0300
bigimg: /assets/img/201802_Lapland/DSC_0043_40_cut.jpg
image: /assets/img/201802_Lapland/DSC_0083_cube.jpg
tags: [travel]
---

When coming to Finland as a foreigner, Lapland is of course one thing you want to visit.
You imaging it in your head as the rough and cold part in northern Finland.
With lots of reindeer, huskies, northern lights and snow.
So the desire to go there was big for me.
The problem though is, that it is quite expensive to go there.
There are some offers from the European student association in Aalto, but it seems to be possible to do it much cheaper if organized by yourself in a large group.

So Fingal and I decided, to ask the new international student, coming in January, if some are interested.
Luckily we found a big group consisting of Santeri, Marius, Jane, Raul, Api, Miki, Yulia, my German friend Andi and myself.
So planing could start.
We booked a small cottage near Saariselkä for almost nothing and rented a mini-bus, soon to be lovingly called party-bus.

## Let's do a Lapland
The excitement is big.
I am quite tense, so hopefully everything will work out.
I just want to sit in our party-bus and drive through the Finnish night.
Santeri and I are the drivers.
We pick up the car in the afternoon.
It’s the first car rental for me, but it goes surprisingly easy.
There it is our party-bus.
We love it! You can literally stand in the back.
We drive a little bit through Helsinki, then to Otaniemi, the campus area, to pick up everybody and hit the road at approx 9pm.
We are aiming to arrive at 4pm to next day at the cottage.
Sleeping, listing to our extra-universal playlist “Zipsöka” and talking fills the hours.

## The party-bus era
The first stop is Myllymäki-town at 1:30.
We pick up Miki at his parents'.
We are welcomed by the best midnight snack you could imagine.
Self-made bread and Finnish cake are only two things you could find on the table.
After 1h it's time for the road again and it's a long road.
The world seems to consist only of the road and the trees in the light cone of our headlights in front of us.
There are no stars and almost no other cars.
You can really feel the big emptiness of Finland’s countryside.
Somehow we make it through the night.
We have one stop close to Oulu in the morning and take a little walk on a frozen lake.
It doesn't feel like I should be still awake.

![grumpy Santeri]({{ site.url }}/assets/img/201802_Lapland/IMG_20180213_080751261_small.jpg )

Finny is in the most grumpy mood I have every experienced.
At 12:30pm we arrive in Rovaniemi only 2.5h later then planed, but anyway who cares.
We have lunch and go to the Arktikum museum.
It gives you a lot of information about the animals living in the northern region and the life of people here.
There is also an exhibition about arctic dating and mating, it is “interesting”.
We hop in the bus again, make a 1h shopping in Lidl and start the last part of our journey.
This period is marked by a lot of small incidents that moved our arrival and the beds in the cottage in distance.
There are some people, who want to drink some alcohol, as a consequence they have to go pee every half an hour.
Then we almost forget Api after one break, only some screams can prevent me from just driving away.
We have to make a 1h detour as one road is closed for what ever reason.
We also take a few times the wrong road, but in the end the sound system of our bus helps us to survive.
Eventually we reach Saariselkä, where we pick up the keys and drive to the cottage.
We are quite positive surprised by the cottage.
It is nice and cozy and looks much better then on the pictures in the internet.
But now is no time to enjoy it, it is time to sleep!

## Lapland freshmen have fun in the snow
The day starts with a nice and long breakfast.
A big shout-out to our breakfast-team and in general shout-out to the different food-teams.
We want to go snowshoe hiking in the national park close to our place.
After everyone has showered it is already noon when we jump in the party-bus once again.
After only a 10 minutes drive we are already at the snowshoe rental place.
Everybody gets a pair and on the track we go.
Snowshoes are some big, flat, metal-plastic things, which you can clip under your shoes, so you can go in the deep snow without sinking in.
So let’s climb a fell, which are called tunturi in Lapland.
All the trees are covered in a thick crust of snow and ice, which is formed by the consistent icy wind.
It is wonderful.

![motivated Finny]({{ site.url }}/assets/img/201802_Lapland/DSC_5548_50.jpg )

As we go further up the tunturi, there are less and less trees and more wind.
While having a lunch break on the fellside, we loose our friend the plastic bag to the wind.
Going further up the beautiful view over Lapland turns into the whiteness of a small snowstorm.
You can turn in any direction, up and down, left and right, everything is white.
But somehow we reach the summit, puhh.
While going back down we have a lot of fun going through +1 meter deep snow.
Even with snowshoes this is difficult and you find yourself sometimes just stuck and falling over.
In the end the hole hike is around 5.6 km.
Back at the cottage we have the most classy looking dinner(risotto) you could imagine in a cottage, it is superb.
Then we deflower the slope of the driveway leading to our cottage, by sledging down on it, in sledges.
Santeri has the great idea to build a tunnel through the big pill of snow.
First there are some doubts, but in the end we make it.
Now it’s sauna-time with jumping in the snow afterwards or sledging through the freshly build tunnel.
We kind of feel super good.

![The tunnel builders]({{ site.url }}/assets/img/201802_Lapland/DSC_5587_50.jpg )

## The day of arrival
I never mentioned it but Yulia and Andi aren’t with us so far.
Today both are coming by plane to Ivalo.
In the morning Marius and I get Yulia from Saariselkä, she came by bus from the airport there.
We have a nice breakfast at the cottage then leave to do some cross country skiing.
For 15€ you can get a pair for the hole day.
The first steps on the track are super strange, especially if some people drive away super fast and leave the noobs behind.
But eventually we all get better and enjoy it.
Some lunch and coffee make it event better.
Going on we are hoping to finally find the slope that is going down.
It seems that we only drive uphill.
For sure.
Finally there is one, which leads in the fabulous arrival of Jane and Raul.
They decide to brake in a manner involving the hole body, quite innovative.
It is fun.
We drive back to the cottage and drop of most of the people, so our dinner, the “spödari”, can be made.
Santeri and I drive to the airport of Ivlao to pick up Andi.
Yeah, it’s true Andi made it to Lapland.
It is surreal to meet him again here in this small airport, somewhere in nowhere.
In the cottage we have the famous Finnish hangover food introduced above.
Then we plan tomorrow, get some sauna and drink some drinks.
Hyvä yötä!

![We the cross country skiers]({{ site.url }}/assets/img/201802_Lapland/DSC_0102_35.jpg )

## Norway just for fun
We have to get up again way to early.
We want to go to Norway today.
Hopefully we see some sunset over the Arctic Ocean.
It’s a freaking long ride up there, so we have to go early.
We are now 10 people, but the bus has only room for 9 persons.
Ergo there’s a problem.
Yulia doesn’t want to drive so far anyway, so we drop her off at Saariselkä’s tourist office.
It's a solution though it doesn't feel like a good solution.
Anyway it’s time to drive now.
The first stop is the Saami museum in Inari.
In one word: It is nice.
Nothing special, but nice.
You can learn a lot about the Saami culture, but we don't have enough time to see everything, as we want to reach the Arctic ocean at sunset time.
We have a good lunch in the museum's restaurant.
The bread is soooo awesome.
Now we drive on in direction north.
On the way there is a big excitement, as some reindeer cross the street.
We take some pictures than we go on.
After a short time there is a bummer.
Thanks to our good friend ©GoogleMaps we manage to drive completely wrong.
After a new turn we are back on the road, but then there is a curve and somehow we end up in the deep snow, not able to move anymore.
We are stuck.
So first let’s keep a cool head.
Our emergency man Miki is activated and he begins to evaluate the situation.
I do some calls to get some official help.
The hole group turns super active and tries to dig the the car free.
On the map we find a neighour and ask for help for pulling us out.
As you can(*can’t) expect, the woman living there can only speak French...in the middle of Lapland...only French...Api’s skills are needed.
In the end the communication works.
In the first try of pulling us out the rope rips apart.
In the second try, with a new rope and a adrenaline level of 150%, we manage to get free.
The joy was super, mega huge.
Ok, keep calm and continue driving.
We only lost 1h.
At some point we are in Norway, there are some mountains, it is quite pictures.
Let's count: 22, 23, 24, 25, there is the  Arctic Ocean! Noice!
We reach our destination Bugøynes, sadly it’s already dark, so no sunset.
The village is super small, but there is a unsupervised camping place close to a small sand beach.
We make a fire #supernice, followed by a dinner in the coldness #supercold.
While walking on the beach we discover, that by stamping  on the sand it starts to glow.
Some plankton is involved in this process.
We make a big stepping festival and light up the hole beach.
Wonderful.
It’s getting really cold, so we drive back to the cottage.
Too bad that even here we didn’t see any northern lights.

![Norway times]({{ site.url }}/assets/img/201802_Lapland/IMG_6755.JPG )

## It's sledging time
No concrete plans for today.
So we can have a long sleep and then just go to Saariselkä and ask at the tourist office, if we can go pet some reindeer.
Sadly we can't.
You can only do it, if you book some reindeer ride, which is ridiculously expensive.
As we are poor students we skip.
This leaves us with hiking on the snowshoe track, some sledges on ours coattail.
Andi's childish joy about the deep snow is hilarious.
We climb up the fell and have a food break on the top.
Then it is time for sledging down, back to Saariselkä while it’s getting dark.
Going down the narrow pathes, in a small plastic sledge often two persons in one together is quite challenging, but also all the better.
Back home there is a big cooking party.
Pizza is made for the drive back and pasta for this evening.
With a small dancing session, the last sauna in Lapland and some card games we celebrate the last evening here.
Now let’s go sleep, it’s gonna be a long ride tomorrow.
![Sledging]({{ site.url }}/assets/img/201802_Lapland/DSC_0094_35.jpg )

## Afterlife
The last breakfast from our breakfast-team.
Then pack all our stuff and clean everything.
Around 12pm we jump in the now so called After-Party-Bus and we drive, drive and drive.
On the way we drop of Yulia, she’s going to fly back to Helsinki by plane.
There is a glorious stop in the Santa-Claus village near Rovaniemi.
It is a tourist trap in its finest form, you can see Santa, the real Santa, there everyday a week from 10-17 and make nice photos with him.
The hole thing is also live video streamed to the internet.
It feels a little bit wrong, so we go again.
We eat our pizza from yesterday, watch how the temperature decreases while we drive further south, which is everything but logical and have some dinner in some Kebab-pizza place, which is ranked 6th on some online ranking, concerning restaurants in Oulu.
At some point in the night we stop in Myllymäki-town to drop of some stuff of Miki.
We are once again welcomed by some delicious midnight snacks.
Mum Myllymäki is the best.
We have now been two times to Miki's.
In both cases it was around 1 to 2am, just a sidenote.
At somewhat like 6:30am we arrive back in Otaniemi, Andi, Raul and I are back at Kannelmäki at something like 7:30am.
Sleep now!
It was a crazy, awesome and restless trip! I liked it!
