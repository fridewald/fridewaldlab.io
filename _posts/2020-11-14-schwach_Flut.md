---
title: "Schwache Flut"
tags: [poem]
---

Wüste und Ozean  
zwei in einem  
Wellen breiten sich sanft  
bis zum Horizont  
{: style="text-align: center;"}

Sanft schwappt das Wasser  
an den Strand  
Die Flut kommt  
holt sich ein bisschen zurück  
{: style="text-align: center;"}

Sie sind zwei Seiten  
unveränderbar und mächtig  
in einem allein  
kommt der Wunsch nach dem anderen  
{: style="text-align: center;"}

Die Wellen breiten sich aus  
so lebendig und doch tot  
An der Küste ist alles zu Ende  
und fängt an  
{: style="text-align: center;"}

Das dumpfe Gefühl des Mundes  
Das Salz auf den Lippen  
Die Sonne hat dich gelähmt  
der Sturm frisst dich auf  
{: style="text-align: center;"}

Die Vögel fliegen drüber  
ein festes Ziel vor Augen  
blind für Sand und Dünen  
etwas zieht sie  
{: style="text-align: center;"}

Strandgut herbei gespült  
durch schwache Flut  
Ein Fuß im Wasser  
einer im Sand  
{: style="text-align: center;"}

Die Sonne geht unter  
einfach weiterlaufen
{: style="text-align: center;"}
