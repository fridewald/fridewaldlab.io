---
layout: post
title: "it's spring time!"
date: 2018-04-06 11:30:00 +0300
#bigimg: /assets/img/201801_Finland/2017-09-17_17-46-41_clipping_small.jpg
tags: [travel, thoughts]
---
It finally happened.
We have spring here in Finnland.
At least theoretically.
This year the snow must have missed the note.
What does spring mean here in Finnland though?
It basically means that there is sunlight.
The sun slowly sneaks in, you wouldn't even notice the change at first, but then suddenly there are more the 12 hours of it.
There are days at which you can actually feel the warmth of the sun on your skin, imagine it.
It's beautiful.
One the other hand your body also thinks: "F**k, it is spring, what should I do?" In German there is a word called "_Frühjahrsmüdigkeit_", it means something like "_tiredness of spring_", and describes the feeling of powerlessness and lack of motivation some people have while spring.
They say it has to do with the change of hormones.
Anyway I think it's true for me, as the change of winter to spring is especially drastic here.
But let's go outside and enjoy the sun!
It's a few degrees above zero, which actually feels warm after a winter with up to minus 20°C.
Maybe that'll bring some "_Frühlingsgefühle_" and defeat the tiredness.

Even the snow is slowly melting.
In the end it got the note too.
