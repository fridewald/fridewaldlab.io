---
title: "Kleines Haus"
tags: [poem]
---

Ein kleines Haus auf einem Berg  
man kann es schmücken, dekorieren  
für manche ist es groß,  
für andere klein  
die Türen sind offen  
für jeden mit einem Schlüssel  
aber manchmal  
tritt der Sturm sie ein.  
Regen und Schnee zerren an ihm,  
aber es fällt nicht so leicht zusammen.  
Im Sommer ist es so schön  
morgens raus schauen ist ein Traum  
alles lebt draußen  
es ist gemacht um es zu genießen,  
doch in der Nacht ist es anders,  
nichts ist da wo es seien sollte  
man kann sich verlaufen  
manchmal habe ich das Gefühl  
es spukt ein bisschen,  
Sachen bewegen sich,  
ohne das man es will  
es muss irgendetwas sein,  
irgendetwas aus der Vergangenheit  
doch ich liebe es  
mein Haus auf dem Berg  
{: style="text-align: center;"}
