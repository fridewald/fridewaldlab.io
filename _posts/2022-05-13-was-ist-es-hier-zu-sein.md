---
title: "Was ist es hier zu sein?"
tags: [poem]
---
Du hast deine Gesichter,  
aber der Spiegel ist gebrochen.  
Sie sind nicht sichtbar  
einfach nur grau.
{: style="text-align: center;"}

Die Herzen der Anderen sind stark  
aber sie sind weit draußen  
erst wenn es zu Hause leuchtet  
macht es Sinn
{: style="text-align: center;"}

Es gibt diesen Zustand des Lebens  
doch er ist leer  
leer, gefüllt mit vergessen  
mit tun und tun
{: style="text-align: center;"}

Manchmal strahlt es wunderschön  
Es sind die Übergänge  
sie sind beides  
subtile Freude und kurzer Schmerz  
{: style="text-align: center;"}