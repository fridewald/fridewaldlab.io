---
layout: page
title: About me
---

I was physics MSc student in Karlruhe, now I live in Munich.
I like going outside as much as staying inside and am always curious about understanding how the world works.
